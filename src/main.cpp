#include <iostream>
#include <chrono>
#include <thread>
#include <gtkmm.h>
using namespace std;
Gtk::Dialog* diptr = nullptr;


int show_warn() {
		cout << "runn" << endl;
		int response = diptr->run();
		if (response == -7) {
			diptr->hide();
		}
		cout << response << endl;
		return 10;
}

int main(int argc, char** argv) {

	auto app = Gtk::Application::create(argc, argv, "us.tremolo.rim");
	app->hold();
	// Create status icon and dialog
	auto icon = Gtk::StatusIcon::create("document-save");
	Gtk::MessageDialog dialog("Relaxation in Moderation", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_CLOSE);
	dialog.set_secondary_text("It's been 30 minutes. Take a 5 minute break.");
	dialog.hide();
	diptr = &dialog;
	sigc::connection conn = Glib::signal_timeout().connect(sigc::ptr_fun(&show_warn), 1800000);
	cout << "RIM started." << endl;

	return app->run();
}
